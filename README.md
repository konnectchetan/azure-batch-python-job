## Instructions
* Clone the code
    * git clone https://gitlab.com/connectchetan/azure-batch-python-job
    * cd azure-batch-python-job/src
* Upgrade PIP & Install libraries
    * pip3 install --upgrade pip3
    * pip3 install -r requirements.txt
* Update the respective values in config.py
* Execute the code
    * python3 batch_python_tutorial_ffmpeg.py 
